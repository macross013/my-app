import React from 'react';
import { BrowserRouter, Switch ,Route } from 'react-router-dom';

import Recipe from './component/recipe';
import Report from './component/report/';
import Login from './component/login';
import MasterPage from './component/master_page';

const Router = () => (
<BrowserRouter >
     <Switch>
     	<Route path="/" component={Login} exact />
 		<Route path="/admin/" component={Login} />
 		<MasterPage>
 			<Route path="/recipe/:id" component={Recipe} />
 			<Route path="/report" component={Report} />
 		</MasterPage>
 	</Switch>
 </BrowserRouter>
);

export default Router;