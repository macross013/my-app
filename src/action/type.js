export const LOGIN_SUCCESS = "login_success";
export const LOGIN_ERROR = "login_error";
export const LOGIN_WAIT = "login_wait";

export const ROOT_URL = "https://fromas.online:8887/";