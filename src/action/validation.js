// import _ from 'loadash'; Load all function from loadash
import { isEmpty } from 'lodash';


export function loginValidate(dataSerialize){
	console.log("Validate",dataSerialize);
	const error = {};

	if(isEmpty(dataSerialize.username)){
		error.username = "Please enter your username";
	}
	if(isEmpty(dataSerialize.password)){
		error.password = "please enter your password";
	}

	return error;
}

