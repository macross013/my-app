import axios from 'axios';
import md5 from 'md5';
import { Redirect } from 'react-router-dom';
import {LOGIN_SUCCESS , LOGIN_ERROR , LOGIN_WAIT ,ROOT_URL} from './type';

function logInError(message){
	return{
		type:LOGIN_ERROR,
		payload:message
	}
}

export function logInAction({username,password},history){
	console.log("Login Action ",username,password);
	return function(dispatch){
		dispatch({type: LOGIN_WAIT});

		const data ={
			"user_id": username,
			"password": md5(password),
			"device_name": "Test-from-eak"
		}

		axios.post(`${ROOT_URL}system/login`,data).then(response => {
			console.log("Response",response);
			const {data : {res_code,res_message,result} } = response;
			if(res_code === 0){
				dispatch({
					type:LOGIN_SUCCESS,
					payload:result
				})

				localStorage.setItem('lc_username', username);
				localStorage.setItem('authen', true);
				localStorage.setItem('lc_reports', result.reports);
				localStorage.setItem('lc_hotel_name',result.hotel_name);
				localStorage.setItem('lc_access_token', result.access_token);

				console.log(result.reports);

				/* Foreach 
				for (const [index, value] of result.reports.entries()) {
				  console.log(`${index}: ${value.name}`);
				}*/

				result.reports.map((value, key) => {
			        console.log(`${key}: Report ID = ${value.id} , Name = ${value.name} `);
			     })


				//history.push('/report');

			}else{
				dispatch(logInError(res_message));
			}


		}).catch((error) => {

			dispatch(logInError(`${error}`));

			console.log("Error ",error);
		})
	}
}
