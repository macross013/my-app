import React ,{Component} from 'react';
import {connect} from 'react-redux';
import {Field,reduxForm} from 'redux-form';

import * as action from '../action';
import {loginValidate} from '../action/validation';


class Form extends Component {

		handleFormSubmit(dataSerialize){
			//console.log("Submit Data : ",this.props);
			this.props.logInAction(dataSerialize);
		}

		renderInput(fields){
			const {meta:{touched,error}, input ,label,className , type, placeholder} = fields;
			console.log("Input : ",input);
			return(
					<div className={className}>
						<label>{label}</label>
						<input {...input} type={type} className="form-control" placeholder={placeholder} />
						{ touched && error && <small className="text-danger">{error}</small> }
					</div>
				)
		}

		renderAction(){
			const {login_error,login_success} = this.props.auth;
			if(login_error){
				return (
					<div className="alert alert-danger alert-dismissible">
						<small className="text-danger">{login_error}</small>
					</div>
				)
			}
		}


		render(){
		const { props: {handleSubmit ,auth} , renderInput , handleFormSubmit } = this;
		console.log("action ",auth);
	  	return(
	  		<div className="loginForm">
	  		<form className="form-group" onSubmit={handleSubmit(handleFormSubmit.bind(this))}>
	  		  <Field  name="username" className="form-group" type="text" label="Username" placeholder="Username" component={renderInput} />
	  		  <Field  name="password" className="form-group" type="password" label="Password" placeholder="Password" component={renderInput} />
             {this.renderAction()}

              <button className="btn btn-primary" type="submit" disabled={auth.waiting}>
              { (auth.waiting)?'Please wait':'Log in' }</button>
            </form>
            </div>
            )
        }
	}

	function validate(dataSerialize){
		return loginValidate(dataSerialize);

	}
	function mapStateToProps(state){
		console.log("mapStateToPorps",state);
		return { auth: state.auth};
	}

export default reduxForm({
	form: 'loginForm',
	validate
})(connect(mapStateToProps,action)(Form));