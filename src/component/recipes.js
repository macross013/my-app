import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class Receipes extends Component{

	render(){
		return(
			<div className="container">
            <div className="row">
            	{
					this.props.recipesData.map((recipe) => {
						console.log(" Receipt data : ",recipe);
						return (
							<div key={recipe.recipe_id} className="col-md-4" style={{marginBottom:"2rem"}}>
							<div className="recipes__box">
							<img className="recipe__box-img" src={recipe.image_url} alt={recipe.title} />
							<div className="recipe__text">
							<h5 className="recipes__title">{recipe.title.length < 20 ? recipe.title: recipe.title.substring(0,25)}</h5>
							<p className="recipes_subtitle">Publisher: <span>{recipe.publisher}</span></p>
							</div>

							<button className="btn btn-primary">
								<Link to={{  state:{ recipe: recipe.title , keyID:recipe.recipe_id}, 
								             pathname: `/recipe/${recipe.recipe_id}` 
								         }} >View Recipe</Link>
							</button>
							</div>

							</div>
							)

					})
				}
			</div>
			</div>

		);
	}

}
export default Receipes;