import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

export class MasterPage extends Component {

	render() {
		console.log("MasterPage : ",this.props.children)
		return (
			<div>
				<h1>Header</h1>
				<div>{this.props.children}</div>
				<h2>Footer</h2>
			</div>
		);
	}
}
export default withRouter(MasterPage);
