import {  LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_WAIT } from '../action/type';

export default function(state = {}, action) {
 switch(action.type) {
    case LOGIN_SUCCESS:
    return { ...state, login_success: action.payload, login_error: '',waiting: false
    };
    case LOGIN_ERROR:
    return { ...state, login_success: '', login_error: action.payload,waiting: false
    };
    case LOGIN_WAIT:
    return { ...state, login_success: '', login_error: '', waiting: true
    };
    default:
      return state;
}
}